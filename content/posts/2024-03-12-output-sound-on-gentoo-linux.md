+++
title = "Gentoo Linux で音を出力する"
+++

## はじめに

Gentoo Linux で音を出力する方法について書きます。

Gentoo wiki の ALSA のページを参考にしました。

[ALSA - Gentoo wiki](https://wiki.gentoo.org/wiki/ALSA)

## インストールの準備

### カーネルの設定

まず、カーネルの設定でサウンドカードのサポートが有効になっていることを確認します。

システムに接続されているオーディオデバイスを表示するには、次のコマンドを実行します。

```
user $ lspci | grep -i audio
00:1f.3 Audio device: Intel Corporation Sunrise Point-LP HD Audio (rev 21)
```

ここでは、次のカーネルオプションが有効になっていることを確認しました。

```
SND_HDA_INTEL [=y]
SND_HDA_CODEC_CONEXANT [=y]
```

カーネルの設定を変更した場合は、カーネルをリビルドし、システムを再起動しておきます。

### Portage の設定

次に、Portage の設定で ALSA のサポートが有効になっていることを確認します。

デスクトッププロファイルの場合は、ALSA のサポートがデフォルトで有効になっています。
したがって、デスクトッププロファイルを使用している場合は、Portage の設定を変更する必要はありません。

デスクトッププロファイル以外の場合は、ALSA のサポートが有効になっていません。
デスクトッププロファイルへ切り替える、または `USE` 変数に `alsa` USE フラグを設定することによって、ALSA のサポートを有効にします。

Portage の設定を変更した場合は、次のコマンドを実行して設定の変更をシステムに適用します。

```
root # emerge --ask --update --deep --changed-use @world
```

## インストール

インストールの準備が完了したら、alsa-utils をインストールします。
このパッケージには、alsasound init スクリプトや `alsamixer` などが含まれています。

```
root # emerge --ask media-sound/alsa-utils
```

## 設定

インストールが完了したら、設定をおこないます。

まず、サウンドカードへのアクセス権限を持っていることを確認します。

```
user $ getfacl /dev/snd/controlC0 | grep <username>
user:<username>:rw-
```

サウンドカードへのアクセス権限を持っていない場合は、elogind などのログインデーモンを使用する、またはユーザーを `audio` グループに追加する必要があります。

次に、次回以降のシステム起動時に alsasound が自動的に起動されるように設定しておきます。

```
root # rc-update add alsasound boot
```

その後、`rc-service` コマンドを使用して alsasound を起動します。

```
root # rc-service alsasound start
```

`alsamixer` を起動し、ミュートの解除と音量の調整をします。
操作方法については、`F1` キーを押すと表示されるヘルプを参照してください。

```
user $ alsamixer
```

最後に、`speaker-test` コマンドを使用して音の出力をテストします。
テストを終了するには、`Ctrl` + `C` を押します。

```
user $ speaker-test -t wav -c 2
```
