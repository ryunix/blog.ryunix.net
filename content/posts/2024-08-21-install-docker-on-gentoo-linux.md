+++
title = "Gentoo Linux に Docker をインストールする"
+++

## はじめに

Gentoo Linux に Docker をインストールする方法について書きます。

Gentoo wiki の Docker のページを参考にしました。

[Docker - Gentoo wiki](https://wiki.gentoo.org/wiki/Docker)

## インストール

Gentoo Linux に Docker をインストールするには、二つのパッケージが必要です。

ひとつは `app-containers/docker` です。
このパッケージには、Docker デーモンなどが含まれています。

```
root # emerge --ask app-containers/docker
```

インストールが完了した後に、不足しているカーネルオプションの一覧が表示されます。
この一覧は、あとでカーネルの設定を変更するときに参考になるので、メモしておくとよいでしょう。

もうひとつは `app-containers/docker-cli` です。
このパッケージには、Docker デーモンと対話するための Docker CLI などが含まれています。

```
root # emerge --ask app-containers/docker-cli
```

Docker CLI の `docker build` コマンドで使用されている従来のビルダーは非推奨となっており、将来のリリースで削除される予定です。
そのため、この機能を使用する場合は、`app-containers/docker-buildx` パッケージをインストールするとよいでしょう。
このパッケージをインストールすることにより、従来のビルダーの代わりに新しいビルダーである BuildKit が自動的に使用されるようになります。

```
root # emerge --ask app-containers/docker-buildx
```

## カーネルの設定

Docker を動作させるためには、適切なカーネルの設定が必要です。
`app-containers/docker` パッケージは、カーネルの互換性をチェックするためのシェルスクリプトも提供します。
このシェルスクリプトを実行すると、Docker の動作に関連するカーネルオプションとその状態の一覧が表示されます。

```
user $ /usr/share/docker/contrib/check-config.sh
```

シェルスクリプトの出力を参考にしてカーネルの設定をおこないます。
ただし、使用するカーネルのバージョンや Docker のバージョンなどによって、必要なカーネルオプションは異なる可能性があります。

ここでは、カーネルのバージョン 6.6.47 と Docker のバージョン 26.1.0 を使用しています。
これらのバージョンに基づいて、次のカーネルオプションを有効にしました。

```
MEMCG [=y]
VETH [=y]
BRIDGE [=y]
NETFILTER_ADVANCED [=y]
BRIDGE_NETFILTER [=y]
NETFILTER_XT_MATCH_IPVS [=y]
BPF_SYSCALL [=y]
CGROUP_BPF [=y]

BLK_DEV_THROTTLING [=y]
CFS_BANDWIDTH [=y]
NETFILTER_XT_TARGET_REDIRECT [=y]
IP_VS [=y]
IP_VS_NFCT [=y]
IP_VS_PROTO_TCP [=y]
IP_VS_PROTO_UDP [=y]
IP_VS_RR [=y]
OVERLAY_FS [=y]
```

カーネルの設定が完了したら、カーネルをリビルドし、システムを再起動しておきます。

## Docker の起動

インストールとカーネルの設定が完了したら、Docker を起動して動作を確認します。

まず、`rc-service` コマンドを使用して Docker を起動します。

```
root # rc-service docker start
```

一般ユーザーとして `docker` コマンドを実行できるようにするには、ユーザーを `docker` グループに追加します。
ただし、`docker` グループに追加すると、セキュリティリスクが高まる可能性があるため、注意が必要です。
詳細については、Docker のセキュリティに関する公式ドキュメントを参照してください。

[Docker security | Docker Docs](https://docs.docker.com/engine/security/)

```
root # gpasswd -a <username> docker
```

ユーザーを `docker` グループに追加した場合は、再ログインしてグループの変更を有効にします。

最後に、次のコマンドを実行して Docker が正常に動作していることを確認します。

```
user $ docker run --rm hello-world
```
