+++
title = "Gentoo Linux に X.Org Server をインストールする"
+++

## はじめに

Gentoo Linux に X.Org Server をインストールする方法について書きます。

Gentoo wiki の Xorg/Guide のページを参考にしました。

[Xorg/Guide - Gentoo wiki](https://wiki.gentoo.org/wiki/Xorg/Guide)

## ハードウェアの情報

```
user $ lspci | grep -i vga
00:02.0 VGA compatible controller: Intel Corporation HD Graphics 620 (rev 02)
```

## インストールの準備

### カーネルの設定

まずはカーネルの設定で入力デバイスとビデオカードのサポートが有効になっていることを確認します。

```
INPUT_EVDEV [=y]

FB_EFI [=y]
FRAMEBUFFER_CONSOLE [=y]

AGP_INTEL [=y]
DRM [=y]
DRM_FBDEV_EMULATION [=y]
DRM_I915 [=y]
```

カーネルの設定を変更した場合は、カーネルをリビルドし、システムを再起動しておきます。

### Portage の設定

次に Portage の設定で X のサポートが有効になっていることを確認します。

デスクトッププロファイル以外の場合は、X のサポートが有効になっていません。
`USE` 変数に `X` USE フラグを設定することによって、X のサポートを有効にします。
また、一般ユーザーで X.Org Server を実行する場合は、logind プロバイダーが必要です。
そのため、`elogind` USE フラグもあわせて設定しておきます。

デスクトッププロファイルの場合は、X のサポートがデフォルトで有効になっています。
さらに、logind プロバイダーもすでにインストールされています。
したがって、デスクトッププロファイルを使用している場合は、Portage の設定を変更する必要はありません。

```
root # vim /etc/portage/make.conf
```

```diff
--- a/make.conf
+++ b/make.conf
@@ -1,3 +1,4 @@
+USE="X elogind"
 CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext pclmul popcnt rdrand sse sse2 sse3 sse4_1 sse4_2 ssse3"
 L10N="en"
 
```

Portage の設定を変更した場合は、次のコマンドを実行して USE フラグの変更をシステムに適用します。

```
root # emerge --ask --update --deep --changed-use @world
```

### elogind の設定

続いて次回以降のシステム起動時に、elogind が自動的に起動されるように設定しておきます。

```
root # rc-update add elogind boot
```

設定が終わったら、システムを再起動します。

```
root # shutdown -r now
```

システムが再起動されたら、次のコマンドを実行して elogind が正常に動作していることを確認します。

```
user $ loginctl user-status
```

エラーメッセージが表示された場合は、Gentoo wiki の elogind のページを参照してください。

[elogind - Gentoo wiki](https://wiki.gentoo.org/wiki/Elogind)

### ビデオカードドライバーと入力デバイスドライバーの設定

最後に Portage の設定でビデオカードドライバーと入力デバイスドライバー用の変数を設定します。

`VIDEO_CARDS` がビデオカードドライバー用の変数で、`INPUT_DEVICES` が入力デバイスドライバー用の変数です。
変数が未定義の場合は、プロファイルで定義されたデフォルトの値が設定されています。
現在の設定を確認するには、次のコマンドを実行します。

```
user $ portageq envvar VIDEO_CARDS
amdgpu fbdev intel nouveau radeon radeonsi vesa dummy v4l
user $ portageq envvar INPUT_DEVICES
libinput
```

適切なドライバーを変数に設定します。

```
root # vim /etc/portage/make.conf
```

```diff
--- a/make.conf
+++ b/make.conf
@@ -1,6 +1,7 @@
 USE="X elogind"
 CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext pclmul popcnt rdrand sse sse2 sse3 sse4_1 sse4_2 ssse3"
 L10N="en"
+VIDEO_CARDS="intel"
 
 COMMON_FLAGS="-march=native -O2 -pipe"
 CFLAGS="${COMMON_FLAGS}"
```

次のコマンドを実行して USE フラグの変更をシステムに適用します。

```
root # emerge --ask --update --deep --changed-use @world
```

## インストール

インストールの準備が完了したら、X.Org Server をインストールします。

```
root # emerge --ask x11-base/xorg-server
```

インストールが完了したら、環境設定を更新します。

```
root # env-update
```

それから、更新された環境設定を再読み込みします。

```
user $ source /etc/profile
```

## X.Org Server の起動

X.Org Server を起動する前に、twm と xterm をインストールしておきます。
これらのパッケージは、X.Org Server が正常に動作していることを確認するために使用できます。

一時的に使用するパッケージをインストールする場合は、`--oneshot` オプションが便利です。
このオプションを使用してインストールしたパッケージは、次に `emerge --depclean` を実行した場合に削除されます。

```
root # emerge --ask --oneshot x11-wm/twm x11-terms/xterm
```

`startx` コマンドを使用して X.Org Server を起動します。

```
user $ startx
```

いくつかの xterm ウィンドウが表示されたら、X.Org Server が正常に動作しています。

X.Org Server を終了するには、すべての xterm ウィンドウを閉じます。
