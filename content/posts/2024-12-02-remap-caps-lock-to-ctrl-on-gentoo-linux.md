+++
title = "Gentoo Linux で Caps Lock を Ctrl にリマップする"
+++

## はじめに

Gentoo Linux で Caps Lock を Ctrl にリマップする方法について書きます。

Gentoo wiki の Keyboard layout switching のページを参考にしました。

[Keyboard layout switching - Gentoo wiki](https://wiki.gentoo.org/wiki/Keyboard_layout_switching)

## コンソール用の設定

まず、Caps Lock を Ctrl にリマップするためのカスタムキーマップファイルを作成します。

`/usr/share/keymaps` ディレクトリで提供されている、左 Ctrl と Caps Lock を入れ替えるためのキーマップファイルを参考にしました。

```
user $ zcat /usr/share/keymaps/i386/include/ctrl.map.gz
keycode  29 = Caps_Lock
keycode  58 = Control
keycode  97 = Compose
```

このキーマップファイルには、左 Ctrl を Caps Lock にリマップする定義や、Caps Lock を Ctrl にリマップする定義が含まれています。
これから作成するカスタムキーマップファイルでは、Caps Lock を Ctrl にリマップする定義をそのまま使用します。
ただし、キーコードはシステムによって異なる可能性があるため、`dumpkeys` コマンドや `showkey` コマンドを使用して、実際のキーコードを確認しておくとよいでしょう。

次のコマンドを実行してカスタムキーマップファイルを作成します。
カスタムキーマップファイルには任意のファイル名を指定できます。

```
user $ echo 'keycode  58 = Control' > nocaps.map
user $ gzip nocaps.map
```

次に、作成したカスタムキーマップファイルを `/usr/share/keymaps` ディレクトリ以下の任意のディレクトリに移動します。
ここでは、参考にしたキーマップファイルと同じディレクトリにカスタムキーマップファイルを移動しました。

```
root # mv nocaps.map.gz /usr/share/keymaps/i386/include/
```

最後に、`/etc/conf.d/keymaps` ファイルを編集して `extended_keymaps` 変数に作成したカスタムキーマップ名を設定します。

```
root # vim /etc/conf.d/keymaps
```

```diff
--- a/keymaps
+++ b/keymaps
@@ -9,7 +9,7 @@ keymap="us"
 windowkeys="YES"
 
 # The maps to load for extended keyboards.  Most users will leave this as is.
-extended_keymaps=""
+extended_keymaps="nocaps"
 #extended_keymaps="backspace keypad euro2"
 
 # Tell dumpkeys(1) to interpret character action codes to be
```

これにより、次回以降のシステム起動時にカスタムキーマップファイルが読み込まれて、Caps Lock が Ctrl にリマップされます。

設定の変更をすぐに適用したい場合は、keymaps サービスを再起動します。

```
root # rc-service keymaps restart
```

## X.Org Server 用の設定

X.Org Server 環境で Caps Lock を Ctrl にリマップする方法はいくつかありますが、ここでは X.Org Server の設定ファイルを使用します。
この方法は、デスクトップ環境を使用せず、ウィンドウマネージャーのみを使用する場合に推奨されています。

まず、設定ファイルを格納するためのディレクトリを作成します。

```
root # mkdir /etc/X11/xorg.conf.d
```

次に、Caps Lock を Ctrl にリマップするための設定ファイルを作成します。
設定ファイルには任意のファイル名を指定できますが、ファイル名は .conf で終わる必要があります。

```
root # vim /etc/X11/xorg.conf.d/10-keyboard.conf
```

```
Section "InputClass"
	Identifier "keyboard-all"
	MatchIsKeyboard "on"
	Option "XkbOptions" "ctrl:nocaps"
EndSection
```

これにより、次回以降の X.Org Server 起動時に設定ファイルが読み込まれて、Caps Lock が Ctrl にリマップされます。
