+++
title = "Gentoo Linux に dwm をインストールする"
+++

## はじめに

Gentoo Linux に dwm をインストールする方法について書きます。

Gentoo wiki の dwm のページを参考にしました。

[dwm - Gentoo wiki](https://wiki.gentoo.org/wiki/Dwm)

## dwm について

dwm は X Window System 用の動的ウィンドウマネージャーです。
タイルレイアウトやフローティングレイアウトなどでウィンドウを管理します。
これらのレイアウトを動的に適用することによって、使用するアプリケーションと実行するタスクに応じて環境を最適化できます。

dwm は起動時に読み込まれる設定ファイルや設定画面からの設定変更をサポートしていません。
そのため、カスタマイズをおこなうには、ソースコードを編集する必要があります。
ソースコードを編集することによって、より深いレベルでのカスタマイズが可能になります。

## インストールの準備

dwm をインストールする前に、X.Org Server をインストールしておきます。

Gentoo Linux に X.Org Server をインストールする方法については、前に書きました。

[Gentoo Linux に X.Org Server をインストールする](@/posts/2023-12-31-install-xorg-server-on-gentoo-linux.md)

次に、デスクトッププロファイルへ切り替えます。

利用可能なプロファイルの一覧を表示するには、次のコマンドを実行します。

```
user $ eselect profile list
Available profile symlink targets:
  [1]   default/linux/amd64/17.1 (stable) *
  [2]   default/linux/amd64/17.1/selinux (stable)
  [3]   default/linux/amd64/17.1/hardened (stable)
  [4]   default/linux/amd64/17.1/hardened/selinux (stable)
  [5]   default/linux/amd64/17.1/desktop (stable)
```

それから、適切なデスクトッププロファイルを選択します。

```
root # eselect profile set default/linux/amd64/17.1/desktop
```

必要に応じて USE フラグを調整し、次のコマンドを実行してプロファイルの変更をシステムに適用します。

```
root # emerge --ask --update --deep --changed-use @world
```

## インストール

インストールの準備が完了したら、dwm をインストールします。

```
root # emerge --ask x11-wm/dwm
```

次に、追加のパッケージをインストールします。

ひとつは dmenu です。
dmenu は X Window System 用の動的メニューで、アプリケーションランチャーとして機能します。

```
root # emerge --ask x11-misc/dmenu
```

もうひとつは st です。
st は X Window System 用のシンプルなターミナル実装です。

```
root # emerge --ask x11-terms/st
```

## dwm の起動

dwm を起動する前に、`~/.xinitrc` ファイルを作成しておきます。
このファイルは、X Window System のセッションが `startx` コマンドによって初期化される際に実行されるシェルスクリプトです。

`/etc/X11/xinit/xinitrc` ファイルを参考にして、`~/.xinitrc` ファイルを作成しました。

```
user $ vim ~/.xinitrc
```

```sh
#!/bin/sh

if [ -d /etc/X11/xinit/xinitrc.d ]; then
	for f in /etc/X11/xinit/xinitrc.d/*; do
		[ -x "${f}" ] && . "${f}"
	done
	unset f
fi

exec dwm
```

`startx` コマンドを使用して dwm を起動します。

```
user $ startx
```

dwm の使い方については、[チュートリアル](https://dwm.suckless.org/tutorial/)を参照してください。

## dwm のカスタマイズ

Gentoo Linux では、dwm をカスタマイズする方法は主に二つあります。

ひとつは savedconfig を使用する方法です。
この方法では、`/etc/portage/savedconfig` ディレクトリに保存された設定ファイルを編集するものです。
パッケージが更新されるときに、編集した設定ファイルが適用されます。
この方法を使用するには、`savedconfig` USE フラグを有効にする必要があります。

もうひとつは patches を使用する方法です。
この方法では、`/etc/portage/patches` ディレクトリにパッチファイルを配置するものです。
パッケージが更新されるときに、配置したパッチファイルが適用されます。
この方法を使用する場合は、特別な設定は不要です。

それぞれの方法には、特性と利便性があります。
それらの特性と利便性を理解し、使用する方法を選択するとよいでしょう。

それでは、patches を使用して dwm をカスタマイズする方法について説明します。

まずはパッチファイルを作成します。
このパッチファイルは、`MODKEY` の値を `Mod1Mask` から `Mod4Mask` に変更するものです。
`MODKEY` は、dwm のキーバインドにおける修飾キーを定義します。
ここで、`Mod1Mask` は Alt キーを、`Mod4Mask` は Super キー (Windows キー) を表します。

パッチファイル名は .patch または .diff で終わる必要があります。

```patch
--- a/config.def.h
+++ b/config.def.h
@@ -45,7 +45,7 @@ static const Layout layouts[] = {
 };
 
 /* key definitions */
-#define MODKEY Mod1Mask
+#define MODKEY Mod4Mask
 #define TAGKEYS(KEY,TAG) \
 	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
 	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
```

次に、パッチファイルを配置するためのディレクトリを作成します。

```
root # mkdir -p /etc/portage/patches/x11-wm/dwm
```

最後に、作成したパッチファイルを配置し、dwm を再インストールします。

```
root # emerge --ask x11-wm/dwm
```

以上が、patches を使用して dwm をカスタマイズする方法についての説明です。
