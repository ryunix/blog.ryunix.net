+++
title = "Gentoo Linux で日本語を入力する"
+++

## はじめに

Gentoo Linux で日本語を入力する方法について書きます。

Gentoo Linux で日本語を入力するには、インプットメソッドフレームワークとインプットメソッドエディターという二つのソフトウェアが必要です。
この記事では、インプットメソッドフレームワークとして Fcitx を、インプットメソッドエディターとして Mozc を使用します。

Gentoo wiki の Fcitx のページを参考にしました。

[Fcitx - Gentoo wiki](https://wiki.gentoo.org/wiki/Fcitx)

## インストール

まず、Fcitx をインストールします。

```
root # emerge --ask app-i18n/fcitx
```

次に、追加のパッケージをインストールします。

ひとつは Qt 5 用のインプットメソッドモジュールです。
これにより、Qt 5 アプリケーションでも日本語入力が可能になります。

```
root # emerge --ask app-i18n/fcitx-qt5
```

もうひとつは設定ツールです。
これを使用することにより、Fcitx の設定を簡単に変更できます。

```
root # emerge --ask app-i18n/fcitx-configtool
```

その後、Mozc をインストールします。
Mozc をインストールするには、`fcitx4` USE フラグを有効にする必要があります。

```
root # mkdir /etc/portage/package.use/app-i18n
root # echo 'app-i18n/mozc fcitx4' > /etc/portage/package.use/app-i18n/mozc
root # emerge --ask app-i18n/mozc
```

最後に、日本語フォントをインストールします。
日本語フォントはいくつかの選択肢がありますが、ここでは IPA フォントをインストールします。

```
root # emerge --ask media-fonts/ja-ipafonts
```

## 設定

インストールが完了したら、設定をおこないます。

まず、Fcitx を使用するための環境変数を設定します。

`startx` コマンドを使用して X.Org Server を起動する場合は、`~/.xinitrc` ファイルで環境変数を設定します。
上記以外の場合は、適切なファイルで環境変数を設定する必要があります。

また、Fcitx が自動的に起動しない環境では、Fcitx をバックグラウンドで起動する設定も必要です。

```
user $ vim ~/.xinitrc
```

```diff
--- a/.xinitrc
+++ b/.xinitrc
@@ -7,4 +7,9 @@ if [ -d /etc/X11/xinit/xinitrc.d ]; then
 	unset f
 fi
 
+export GTK_IM_MODULE=fcitx
+export QT_IM_MODULE=fcitx
+export XMODIFIERS=@im=fcitx
+
+fcitx &
 exec dwm
```

すでに X Window System のセッションが実行中の場合は、既存のセッションを終了してから新しいセッションを開始する必要があります。

次に、Fcitx の設定ツールを起動してインプットメソッドに Mozc を追加します。

```
user $ fcitx-configtool
```

設定が完了したら、デフォルトのショートカットキーである `Ctrl` + `Space` を使用して、インプットメソッドを切り替えることができます。
