+++
title = "SystemRescue を USB メモリにインストールする"
+++

## SystemRescue について

はじめに [SystemRescue](https://www.system-rescue.org/) の特徴を簡単に紹介します。

- 破損したシステムやデータを修復するために利用できる Linux ディストリビューション。
- 多くのユーティリティソフトウェアが付属している。
- CD/DVD または USB メモリにインストールして使用する。

という感じです。

現在は SystemRescue という名前ですが、バージョン 7.00 より前は SystemRescueCd という名前でした。
また、現在は Arch Linux をベースにしていますが、バージョン 6.0.0 より前は Gentoo Linux をベースにしていました。

## インストールの準備

まずは次の要件をすべて満たす USB メモリを用意します。

- データがすべて削除されるので、重要なデータが含まれていないこと。
- 記憶容量が 2 GB 以上あること。

用意ができたら、次のページから .iso ファイルをダウンロードします。
あわせて .iso ファイルの署名を検証するために必要な .asc ファイルと公開鍵もダウンロードしておきます。

[SystemRescue - Download](https://www.system-rescue.org/Download/)

GNU Privacy Guard (GnuPG または GPG) を使用して、ダウンロードした .iso ファイルの署名を検証します。

```
user $ gpg --import gnupg-pubkey.pem
user $ gpg --verify systemrescue-x.y-amd64.iso.asc systemrescue-x.y-amd64.iso
```

GnuPG が使用できない場合は、チェックサムによる検証をおこないます。

```
user $ sha256sum --check systemrescue-x.y-amd64.iso.sha256
```

いずれかの方法で検証ができたら、インストールの準備が完了です。

## インストール

インストールする方法はいくつかの選択肢がありますが、今回は dd を使用します。
そのほかの方法は次のページをご覧ください。

[SystemRescue - Installing SystemRescue on a USB memory stick](https://www.system-rescue.org/Installing-SystemRescue-on-a-USB-memory-stick/)

dd を使用するときは細心の注意を払います。
もし誤ったデバイスを指定した場合は、データが破壊される可能性があります。

1. 用意した USB メモリを接続します。
2. USB メモリがマウントされている場合は、アンマウントしておきます。
3. `lsblk` コマンドを実行して、USB メモリのデバイス名を識別します。
4. dd を使用して .iso ファイルを USB メモリにコピーします。

```
root # dd if=systemrescue-x.y-amd64.iso of=/dev/sdx bs=128k
```

コマンドが正常終了したら、インストールの完了です。
