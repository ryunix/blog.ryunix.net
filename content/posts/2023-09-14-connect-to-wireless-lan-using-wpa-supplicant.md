+++
title = "wpa_supplicant を使用して無線 LAN に接続する"
+++

## はじめに

wpa_supplicant を使用して無線 LAN に接続する方法について書きます。

Gentoo wiki の wpa_supplicant のページを参考にしました。

[wpa_supplicant - Gentoo wiki](https://wiki.gentoo.org/wiki/Wpa_supplicant)

## 前提条件

- カーネルの設定でワイヤレスサポートと必要なワイヤレスデバイスドライバーが有効になっていること。
- ワイヤレスデバイスドライバーに必要なファームウェアがインストールされていること。
- `net-misc/dhcpcd` と `net-wireless/wpa_supplicant` がインストールされていること。

## 設定ファイルの作成

`wpa_passphrase` コマンドを使用して設定ファイルを作成します。

`wpa_passphrase` コマンドは、引数で指定された SSID とパスフレーズから、設定ファイル用のネットワークブロックを生成し、標準出力に出力します。
パスフレーズの指定を省略した場合は、標準入力からの入力になります。

次の例は、パスフレーズの指定を省略し、標準入力から入力しています。
こうすることにより、パスフレーズがコマンドの履歴に保存されなくなります。

```
user $ wpa_passphrase ssid
# reading passphrase from stdin
passphrase
network={
        ssid="ssid"
        #psk="passphrase"
        psk=2b1d17284c5410ee5eaae7151290e9744af2182b0eb8af20dd4ebb415928f726
}
```

`wpa_passphrase` コマンドの標準出力をリダイレクトし、設定ファイルを作成します。

```
root # wpa_passphrase ssid > /etc/wpa_supplicant/wpa_supplicant.conf
```

一般ユーザーでコマンドを実行する場合は、`sudo` コマンドと `tee` コマンドを組み合わせて使用します。

```
user $ wpa_passphrase ssid | sudo tee /etc/wpa_supplicant/wpa_supplicant.conf
```

作成した設定ファイルにはパスフレーズが含まれているため、パーミッションを変更しておきます。

```
root # chmod 600 /etc/wpa_supplicant/wpa_supplicant.conf
```

## wpa_supplicant の起動

設定ファイルが作成できたら、wpa_supplicant を起動します。

まずは次回以降のシステム起動時に、wpa_supplicant が自動的に起動されるように設定しておきます。

```
root # rc-update add wpa_supplicant default
```

次に `rc-service` コマンドを使用して wpa_supplicant を起動します。

```
root # rc-service wpa_supplicant start
```

wpa_supplicant が起動したら、無線 LAN に接続できていることを確認します。

```
user $ ping -c 3 www.gentoo.org
```

## 新しいネットワークの追加

新しいネットワークを追加する方法は以下の通りです。

`wpa_passphrase` コマンドの標準出力をリダイレクトし、設定ファイルに追記します。

```
root # wpa_passphrase ssid >> /etc/wpa_supplicant/wpa_supplicant.conf
```

一般ユーザーでコマンドを実行する場合は、`tee` コマンドの `-a` または `--append` オプションを使用します。

```
user $ wpa_passphrase ssid | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
```

wpa_supplicant を再起動して設定ファイルをリロードします。

```
root # rc-service wpa_supplicant restart
```
