+++
title = "初投稿です"
+++

静的サイトジェネレーターの [Zola](https://www.getzola.org/) を利用して、このブログを作成した。

はじめに Zola の特徴を簡単に紹介する。

- Rust で実装されている。
- テンプレートエンジンに [Tera](https://keats.github.io/tera/) を使用する。
- コンテンツは [CommonMark](https://commonmark.org/) で記述する。[^1]
- 依存関係なしで Sass コンパイルやシンタックスハイライトなどの機能を備える。

という感じだ。

そのほかに、かゆいところに手が届くような機能として、リンクチェッカーがある。
これはコンテンツファイル内のすべての外部リンクをチェックし、リンク切れを検出する機能だ。

もっと知りたい人は、簡単なブログを作成する[チュートリアル](https://www.getzola.org/documentation/getting-started/overview/)を読んでほしい。
このチュートリアルで `zola` コマンドの使い方や Tera テンプレートエンジンの雰囲気がわかるはずだ。

おわりにシンタックスハイライトの例として、Rust のソースコードを載せておく。

```rust
fn main() {
    println!("Hello, world!");
}
```

[^1]: 脚注や GitHub Flavored Markdown のテーブルなどが追加でサポートされている。
