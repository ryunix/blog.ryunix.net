+++
title = "Gentoo Linux をインストールする"
+++

## はじめに

Gentoo Linux をインストールする手順について書く。

Gentoo AMD64 ハンドブックを参考にした。

[Gentoo AMD64 Handbook - Gentoo wiki](https://wiki.gentoo.org/wiki/Handbook:AMD64)

## インストールメディアの選択

SystemRescue を使用する。

SystemRescue を USB メモリにインストールする方法については、前に書いた。

[SystemRescue を USB メモリにインストールする](@/posts/2023-06-03-install-systemrescue-to-usb-memory.md)

SystemRescue を起動する前に、セキュアブートを無効にしておく必要がある。

## ネットワークの設定

ネットワークに接続し、IP アドレスが割り当てられていることを確認する。

```
root # ip addr
```

ネットワークをテストする。

```
root # ping -c 3 www.gentoo.org
```

## ディスクの準備

### ディスクのパーティショニング

ディスクのデバイス名を確認する。

```
root # lsblk
```

ディスクをパーティショニングする。

```
root # fdisk /dev/nvme0n1

Welcome to fdisk (util-linux 2.38.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table.
Created a new DOS disklabel with disk identifier 0x07587b34.

Command (m for help): g
Created a new GPT disklabel (GUID: BB59B8DE-2931-1643-99EE-B598043F59A0).

Command (m for help): n
Partition number (1-128, default 1):
First sector (2048-500118158, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-500118158, default 500117503): +512M

Created a new partition 1 of type 'Linux filesystem' and of size 512 MiB.

Command (m for help): t
Selected partition 1
Partition type or alias (type L to list all): 1
Changed type of partition 'Linux filesystem' to 'EFI System'.

Command (m for help): n
Partition number (2-128, default 2):
First sector (1050624-500118158, default 1050624):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (1050624-500118158, default 500117503): +4G

Created a new partition 2 of type 'Linux filesystem' and of size 4 GiB.

Command (m for help): t
Partition number (1,2, default 2):
Partition type or alias (type L to list all): 19

Changed type of partition 'Linux filesystem' to 'Linux swap'.

Command (m for help): n
Partition number (3-128, default 3):
First sector (9439232-500118158, default 9439232):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (9439232-500118158, default 500117503):

Created a new partition 3 of type 'Linux filesystem' and of size 234 GiB.

Command (m for help): p
Disk /dev/nvme0n1: 238.47 GiB, 256060514304 bytes, 500118192 sectors
Disk model: SAMSUNG MZVLW256HEHP-000L7
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: BB59B8DE-2931-1643-99EE-B598043F59A0

Device           Start       End   Sectors  Size Type
/dev/nvme0n1p1    2048   1050623   1048576  512M EFI System
/dev/nvme0n1p2 1050624   9439231   8388608    4G Linux swap
/dev/nvme0n1p3 9439232 500117503 490678272  234G Linux filesystem

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
```

### ファイルシステムの作成

ファイルシステムを作成する。

```
root # mkfs.fat -F 32 /dev/nvme0n1p1
root # mkfs.ext4 /dev/nvme0n1p3
```

スワップパーティションを有効にする。

```
root # mkswap /dev/nvme0n1p2
root # swapon /dev/nvme0n1p2
```

### ルートパーティションのマウント

マウントポイントを作成する。

```
root # mkdir --parents /mnt/gentoo
```

ルートパーティションをマウントする。

```
root # mount /dev/nvme0n1p3 /mnt/gentoo
```

## Gentoo インストールファイルのインストール

### stage tarball のインストール

現在の日付と時刻が正しいことを確認する。

```
root # date
```

ルートファイルシステムのマウントポイントに移動する。

```
root # cd /mnt/gentoo
```

stage tarball をダウンロードする。

```
root # wget https://ftp.iij.ad.jp/pub/linux/gentoo/releases/amd64/autobuilds/current-stage3-amd64-openrc/stage3-amd64-openrc-20230409T163155Z.tar.xz
root # wget https://ftp.iij.ad.jp/pub/linux/gentoo/releases/amd64/autobuilds/current-stage3-amd64-openrc/stage3-amd64-openrc-20230409T163155Z.tar.xz.asc
```

ダウンロードしたファイルが改ざんされていないことを確認する。

```
root # gpg --keyserver hkps://keys.gentoo.org --recv-keys 0xBB572E0E2D182910
root # gpg --verify stage3-amd64-openrc-20230409T163155Z.tar.xz.asc stage3-amd64-openrc-20230409T163155Z.tar.xz
```

stage tarball を展開する。

```
root # tar xpf stage3-amd64-openrc-20230409T163155Z.tar.xz --xattrs-include='*.*' --numeric-owner
```

前のディレクトリに移動する。

```
root # cd -
```

### コンパイルオプションの設定

コンパイルオプションを設定する。

```
root # vim /mnt/gentoo/etc/portage/make.conf
```

```diff
--- a/make.conf
+++ b/make.conf
@@ -1,15 +1,7 @@
-# These settings were set by the catalyst build script that automatically
-# built this stage.
-# Please consult /usr/share/portage/config/make.conf.example for a more
-# detailed example.
-COMMON_FLAGS="-O2 -pipe"
+COMMON_FLAGS="-march=native -O2 -pipe"
 CFLAGS="${COMMON_FLAGS}"
 CXXFLAGS="${COMMON_FLAGS}"
 FCFLAGS="${COMMON_FLAGS}"
 FFLAGS="${COMMON_FLAGS}"
 
-# NOTE: This stage was built with the bindist Use flag enabled
-
-# This sets the language of build output to English.
-# Please keep this setting intact when reporting bugs.
 LC_MESSAGES=C
```

## Gentoo ベースシステムのインストール

### chroot する

ミラーを設定する。

```
root # vim /mnt/gentoo/etc/portage/make.conf
```

```diff
--- a/make.conf
+++ b/make.conf
@@ -4,4 +4,6 @@ CXXFLAGS="${COMMON_FLAGS}"
 FCFLAGS="${COMMON_FLAGS}"
 FFLAGS="${COMMON_FLAGS}"
 
+GENTOO_MIRRORS="https://ftp.iij.ad.jp/pub/linux/gentoo/ https://distfiles.gentoo.org/"
+
 LC_MESSAGES=C
```

Gentoo ebuild リポジトリを設定する。

```
root # mkdir --parents /mnt/gentoo/etc/portage/repos.conf
root # cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
```

DNS 情報をコピーする。

```
root # cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
```

必要なファイルシステムをマウントする。

```
root # mount --types proc /proc /mnt/gentoo/proc
root # mount --rbind /sys /mnt/gentoo/sys
root # mount --make-rslave /mnt/gentoo/sys
root # mount --rbind /dev /mnt/gentoo/dev
root # mount --make-rslave /mnt/gentoo/dev
root # mount --bind /run /mnt/gentoo/run
root # mount --make-slave /mnt/gentoo/run
```

chroot する。

[Installation alternatives - Gentoo wiki](https://wiki.gentoo.org/wiki/Installation_alternatives)

```
root # chroot /mnt/gentoo /bin/env -i TERM=$TERM /bin/bash
root # env-update
root # source /etc/profile
root # export PS1="(chroot) ${PS1}"
```

ブートパーティションをマウントする。

```
root # mount /dev/nvme0n1p1 /boot
```

### Portage の設定

Gentoo ebuild リポジトリのスナップショットをインストールする。

```
root # emerge-webrsync
```

ニュースを読む。

```
root # eselect news list
root # eselect news read
```

適切なプロファイルが選択されていることを確認する。

```
root # eselect profile show
Current /etc/portage/make.profile symlink:
  default/linux/amd64/17.1
```

@world を更新する。

```
root # emerge --ask --update --deep --changed-use @world
```

パッケージが更新された場合は、次のコマンドを実行する。

```
root # emerge --ask --depclean
```

`CPU_FLAGS_X86` を設定する。

```
root # emerge --ask app-portage/cpuid2cpuflags
root # cpuid2cpuflags
root # nano -w /etc/portage/make.conf
```

```diff
--- a/make.conf
+++ b/make.conf
@@ -1,3 +1,5 @@
+CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext pclmul popcnt rdrand sse sse2 sse3 sse4_1 sse4_2 ssse3"
+
 COMMON_FLAGS="-march=native -O2 -pipe"
 CFLAGS="${COMMON_FLAGS}"
 CXXFLAGS="${COMMON_FLAGS}"
```

USE フラグの変更をシステムに適用する。

```
root # emerge --ask --update --deep --changed-use @world
root # emerge --ask --depclean
```

### テキストエディタのインストール

Vim をインストールする。

```
root # emerge --ask app-editors/vim
```

デフォルトのテキストエディタを選択する。

```
root # eselect editor show
EDITOR variable in profile:
  (none)
root # eselect editor list
Available targets for the EDITOR variable:
  [1]   nano
  [2]   ex
  [3]   vi
  [ ]   (free form)
root # eselect editor set vi
```

### タイムゾーンの設定

タイムゾーンを設定する。

```
root # echo 'Asia/Tokyo' > /etc/timezone
root # emerge --config sys-libs/timezone-data
```

`/etc/localtime` をシンボリックリンクに変換する。

```
root # rm /etc/localtime
root # emerge --config sys-libs/timezone-data
```

### ロケールの設定

使用するロケールをアンコメントする。

```
root # vim /etc/locale.gen
```

```diff
--- a/locale.gen
+++ b/locale.gen
@@ -21,7 +21,7 @@
 # yourself instead of re-emerging glibc.
 
 #en_US ISO-8859-1
-#en_US.UTF-8 UTF-8
+en_US.UTF-8 UTF-8
 #ja_JP.EUC-JP EUC-JP
 #ja_JP.UTF-8 UTF-8
 #ja_JP EUC-JP
```

ロケールを生成する。

```
root # locale-gen
```

ロケールを選択する。

```
root # eselect locale show
LANG variable in profile:
  C.UTF8
root # eselect locale list
Available targets for the LANG variable:
  [1]   C
  [2]   C.utf8
  [3]   POSIX
  [4]   en_US.utf8
  [5]   C.UTF8 *
  [ ]   (free form)
root # eselect locale set en_US.utf8
```

環境をリロードする。

```
root # env-update && source /etc/profile && export PS1="(chroot) ${PS1}"
```

`L10N` と `LINGUAS` を設定する。

```
root # vim /etc/portage/make.conf
```

```diff
--- a/make.conf
+++ b/make.conf
@@ -1,4 +1,5 @@
 CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext pclmul popcnt rdrand sse sse2 sse3 sse4_1 sse4_2 ssse3"
+L10N="en"
 
 COMMON_FLAGS="-march=native -O2 -pipe"
 CFLAGS="${COMMON_FLAGS}"
@@ -9,3 +10,4 @@ FFLAGS="${COMMON_FLAGS}"
 GENTOO_MIRRORS="https://ftp.iij.ad.jp/pub/linux/gentoo/ https://distfiles.gentoo.org/"
 
 LC_MESSAGES=C
+LINGUAS="en"
```

USE フラグの変更をシステムに適用する。

```
root # emerge --ask --update --deep --changed-use @world
```

## Linux カーネルの設定

### ファームウェアとマイクロコードのインストール

package.license ディレクトリを作成する。

```
root # mkdir /etc/portage/package.license
```

ファームウェアをインストールする。

```
root # mkdir /etc/portage/package.license/sys-kernel
root # echo 'sys-kernel/linux-firmware linux-fw-redistributable' > /etc/portage/package.license/sys-kernel/linux-firmware
root # emerge --ask sys-kernel/linux-firmware
```

マイクロコードをインストールする。

```
root # mkdir /etc/portage/package.license/sys-firmware
root # echo 'sys-firmware/intel-microcode intel-ucode' > /etc/portage/package.license/sys-firmware/intel-microcode
root # emerge --ask sys-firmware/intel-microcode
```

マイクロコードの cpio アーカイブを生成する。

```
root # iucode_tool -S --write-earlyfw=/boot/early_ucode.cpio /lib/firmware/intel-ucode/*
```

### カーネルの設定とコンパイル

カーネルソースをインストールする。

```
root # emerge --ask sys-kernel/gentoo-sources
```

`/usr/src/linux` シンボリックリンクを作成する。

```
root # eselect kernel show
Current kernel symlink:
  (unset)
root # eselect kernel list
Available kernel symlink targets:
  [1]   linux-6.1.19-gentoo
root # eselect kernel set linux-6.1.19-gentoo
```

シンボリックリンクが作成されたことを確認する。

```
root # ls -l /usr/src/linux
```

システム情報を収集する。

```
root # emerge --ask sys-apps/pciutils
root # lspci -k
root # lsmod
```

カーネルソースディレクトリに移動する。

```
root # cd /usr/src/linux
```

カーネルを設定する。

[Lenovo ThinkPad X1 Carbon 5th generation - Gentoo wiki](https://wiki.gentoo.org/wiki/Lenovo_ThinkPad_X1_Carbon_5th_generation)

```
root # make menuconfig
```

```
BLK_DEV_NVME [=y]

CFG80211 [=m]

IWLWIFI [=m]
IWLMVM [=m]

MCORE2 [=y]

X86_MCELOG_LEGACY [=y]

FB [=y]
FB_EFI [=y]

SND_HDA_CODEC_CONEXANT [=y]
```

カーネルをコンパイルし、インストールする。

```
root # make -j4 && make modules_install
root # make install
```

前のディレクトリに移動する。

```
root # cd -
```

## システムの設定

### ファイルシステム情報

fstab ファイルを作成する。

[tmpfs - Gentoo wiki](https://wiki.gentoo.org/wiki/Tmpfs)

```
root # vim /etc/fstab
```

```diff
--- a/fstab
+++ b/fstab
@@ -27,3 +27,10 @@
 #UUID=58e72203-57d1-4497-81ad-97655bd56494		/		ext4		noatime		0 1
 #LABEL=swap		none		swap		sw		0 0
 #/dev/cdrom		/mnt/cdrom	auto		noauto,ro	0 0
+
+/dev/nvme0n1p1		/boot		vfat		noauto,noatime	0 2
+/dev/nvme0n1p2		none		swap		sw		0 0
+/dev/nvme0n1p3		/		ext4		noatime		0 1
+
+tmpfs			/tmp		tmpfs		nosuid,nodev,noatime,size=4g	0 0
+tmpfs			/var/tmp/portage		tmpfs		nosuid,nodev,noatime,size=8g,mode=775,uid=portage,gid=portage	0 0
```

### ネットワーク情報

ホスト名を設定する。

```
root # echo lambda > /etc/hostname
```

ホスト名を hosts ファイルに追加する。

```
root # vim /etc/hosts
```

```diff
--- a/hosts
+++ b/hosts
@@ -17,6 +17,8 @@
 127.0.0.1	localhost
 ::1		localhost
 
+127.0.0.1	lambda
+
 #
 # Imaginary network.
 #10.0.0.2               myname
```

### システム情報

root のパスワードを設定する。

```
root # passwd
```

## システムツールのインストール

### システムロガー

metalog をインストールし、サービスの自動起動を有効にする。

```
root # emerge --ask app-admin/metalog
root # rc-update add metalog default
```

### cron デーモン

dcron をインストールし、サービスの自動起動を有効にする。

```
root # emerge --ask sys-process/dcron
root # rc-update add dcron default
root # crontab /etc/crontab
```

### 時刻同期

chrony をインストールし、サービスの自動起動を有効にする。

```
root # emerge --ask net-misc/chrony
root # rc-update add chronyd default
```

chronyd を設定する。

```
root # vim /etc/chrony/chrony.conf
```

```diff
--- a/chrony.conf
+++ b/chrony.conf
@@ -1,8 +1,8 @@
 # Use public NTP servers from the pool.ntp.org project.
-server 0.gentoo.pool.ntp.org iburst
-server 1.gentoo.pool.ntp.org iburst
-server 2.gentoo.pool.ntp.org iburst
-server 3.gentoo.pool.ntp.org iburst
+server 0.gentoo.pool.ntp.org iburst auto_offline
+server 1.gentoo.pool.ntp.org iburst auto_offline
+server 2.gentoo.pool.ntp.org iburst auto_offline
+server 3.gentoo.pool.ntp.org iburst auto_offline
 
 # Record the rate at which the system clock gains/losses time.
 driftfile /var/lib/chrony/drift
```

### ファイルシステムツール

ファイルシステムツールをインストールする。

```
root # emerge --ask sys-fs/dosfstools
```

### ネットワークツール

DHCP クライアントをインストールする。

```
root # emerge --ask net-misc/dhcpcd
```

無線ネットワークツールをインストールする。

```
root # emerge --ask net-wireless/wpa_supplicant
```

### 追加のツール

mcelog をインストールし、サービスの自動起動を有効にする。

```
root # emerge --ask app-admin/mcelog
root # rc-update add mcelog default
```

sudo をインストールする。

```
root # mkdir /etc/portage/package.use/app-admin
root # echo 'app-admin/sudo -sendmail' > /etc/portage/package.use/app-admin/sudo
root # emerge --ask app-admin/sudo
```

sudo を設定する。

```
root # mkdir /etc/sudoers.d
root # visudo -f /etc/sudoers.d/10-wheel
```

```diff
--- /dev/null
+++ b/10-wheel
@@ -0,0 +1 @@
+%wheel ALL=(ALL:ALL) ALL
```

tmux をインストールする。

```
root # emerge --ask app-misc/tmux
```

## ブートローダーの設定

### ブートローダーの選択

GRUB をインストールする。

```
root # emerge --ask sys-boot/grub
```

GRUB ファイルを `/boot/grub` ディレクトリにインストールする。

```
root # grub-install --target=x86_64-efi --efi-directory=/boot
```

GRUB の設定ファイルを生成する。

```
root # grub-mkconfig -o /boot/grub/grub.cfg
```

### システムの再起動

chroot 環境を終了する。

```
root # exit
```

すべてのパーティションをアンマウントし、システムを再起動する。

```
root # cd
root # umount -l /mnt/gentoo/dev{/shm,/pts,}
root # umount -R /mnt/gentoo
root # shutdown -r now
```

## インストールの仕上げ

### ユーザー管理

ユーザーを追加する。

```
root # useradd -m -G users,wheel -s /bin/bash ryunix
root # passwd ryunix
```

### ディスクのクリーンアップ

stage tarball を削除する。

```
root # rm /stage3-amd64-openrc-20230409T163155Z.tar.xz*
```
